if __name__ == '__main__':
    # initialize
    cards = []
    for x in range(100):
        cards.append(False)
    # turn
    for n in range(2, 101):
        i = n - 1
        while i < len(cards):
            cards[i] = not cards[i]
            i += n
    # print
    for i, card in enumerate(cards):
        if not card:
            print(i + 1)
